// Ficheros de cabecera
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

int main(int argc, char *argv[])
{
 //Definir variables,estructuras
 struct sockaddr_in server;
 char ip[16];
 int fd, numbytes,puerto;
 char buf[1024];
 char enviar[1024];
 int A[3][3]; //Matriz para tablero de barcos

 system("clear");

    char res='n'; //variable para almacenar la respuesta del jugadorB

    //se llena la matriz con base a las preferencias del jugadorB, si escribe 's' se agrega un barco a esa posición, si no, se deja vacío, al final se muestra su mapa.
    for(int i=0; i < 3; i++){
        for(int j=0; j<3; j++){

            printf("¿Quiere un barco en la coordenada %d,%d?(s/n)\n",i,j);
            scanf("%s",&res);
            if(res=='s'){
                A[i][j]=1;
            }else{
                A[i][j]=0;
            }
        }
    }
    printf("Su mapa quedó así: \n");
    for(int i=0; i < 3; i++){
        for(int j=0; j<3; j++){
            printf("Coordenada %d,%d = %d\n",i,j,A[i][j]);
        }
    }



printf("ingrese la ip del servidor\n");
 scanf("%s",ip);

printf("ingrese el puerto de conexion\n");
 scanf("%d",&puerto);

 //Socket
 if ((fd=socket(AF_INET, SOCK_STREAM, 0))==-1){
 printf("socket() error\n");
 exit(-1);
 }

 //Datos del servidor
 server.sin_family = AF_INET;
 server.sin_port = htons(puerto);
 server.sin_addr.s_addr=inet_addr(ip);
 bzero(&(server.sin_zero),8);

//Conectarse al servidor
 if(connect(fd, (struct sockaddr *)&server,
 sizeof(struct sockaddr))==-1){
 printf("connect() error\n");
 exit(-1);
 }
 //Recibir mensaje de bienvenida
 if ((numbytes=recv(fd,buf,1024,0)) == -1){
 printf("Error en recv() \n");
 exit(-1);
 }
 printf("%s\n",buf);
//Ciclo para enviar y recibir mensajes
while(1){
 //El servidor espera el primer mensaje
 printf("Escribir coordenada en formato X,Y: ");
 scanf("%*c%[^\n]",enviar);
 send(fd,enviar,1024,0);
 if(strcmp(enviar,"salir")==0){
 break;
 }

 //El cliente recibe el mensaje del servidor
 recv(fd,buf,1024,0);
 if(strcmp(buf,"salir")==0){
 break;
 }else if (strcmp(&buf,"¡Ganaste!")==0){
     printf("%s\n",buf);
     break;
 }


    printf("La coordenada recibida de jugadorA es: (%s)\n",buf);//del buffer se lee la coordenada que envío el jugadorA
    if(A[buf[0]-'0'][buf[2]-'0'] == 0){ //si en la coordenada hay un 0 significa que no hay barco, así que continua el juego
        printf("Aqui no hay barco, ¡el jugadorB falló!\n");
    }else{ //si hay un uno se cambia el valor a cero ya que se ha hundido un barco y se recorre el arreglo para comprobar que aun haya más barcos.
        A[buf[0]-'0'][buf[2]-'0'] = 0;
        printf("Te han hundido un barco, ");
        int salir = 1;
        int aux=0;
        for(int i=0; i < 3; i++){
            for(int j=0; j<3; j++){
                if(A[i][j]==1){ //Si se encuentra un uno, el juego continúa
                    printf("pero sigue el juego.\n");
                    salir = 0;
                    aux++;
                    break;

                }

            }
            if(salir == 0){
                break;
            }
        }
        if(aux==0){
            printf("y ¡Has perdido el juego!\n");
            send(fd,"¡Ganaste!",1024,0);
            send(fd,"salir",1024,0);
            break;
        }
    }



}
 close(fd);
}
