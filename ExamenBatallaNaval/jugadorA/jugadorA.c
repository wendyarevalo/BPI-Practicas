// Ficheros de cabecera
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>

//Funcion principal
int main(int argc, char **argv)
{
//Definir variables,estructuras
struct sockaddr_in server;
struct sockaddr_in client;
int fd,fd2,longitud_cliente,numbytes,puerto;
char buf[1024]; //Para recibir mensaje
char enviar2[1024]; //Para enviar mensaje
char enviar[1024]; //Para enviar mensaje
 int A[3][3]; //Matriz para tablero de barcos


system("clear");

    char res='n'; //variable para almacenar la respuesta del jugadorB

    //se llena la matriz con base a las preferencias del jugadorA, si escribe 's' se agrega un barco a esa posición, si no, se deja vacío, al final se muestra su mapa.
    for(int i=0; i < 3; i++){
        for(int j=0; j<3; j++){

            printf("¿Quiere un barco en la coordenada %d,%d?(s/n)\n",i,j);
            scanf("%s",&res);
            if(res=='s'){
                A[i][j]=1;
            }else{
                A[i][j]=0;
            }
        }
    }
    printf("Su mapa quedó así: ");
    for(int i=0; i < 3; i++){
        for(int j=0; j<3; j++){
            printf("Coordenada %d,%d = %d\n",i,j,A[i][j]);
        }
    }

 printf("La direccion del servidor es 172.26.0.2\n\n");
 printf("Por favor introduzca el puerto de escucha: \n\n");
 scanf("%d",&puerto);



server.sin_family= AF_INET;
 server.sin_port = htons(puerto);
 server.sin_addr.s_addr = INADDR_ANY;
 bzero(&(server.sin_zero),8);

 //Definicion de socket
 if (( fd=socket(AF_INET,SOCK_STREAM,0) )<0){
 perror("Error de apertura de socket");
 exit(-1);
 }

 //Avisar al sistema que se creo un socket
 if(bind(fd,(struct sockaddr*)&server, sizeof(struct sockaddr))==-1) {
 printf("error en bind() \n");
 exit(-1);
 }

 //Establecer el socket en modo escucha
 if(listen(fd,5) == -1) {
 printf("error en listen()\n");
 exit(-1);
 }

 printf("SERVIDOR EN ESPERA...\n");
 longitud_cliente= sizeof(struct sockaddr_in);
 if ((fd2 = accept(fd,(struct sockaddr *)&client,&longitud_cliente))==-1) {
 printf("error en accept()\n");
 exit(-1);
 }



 printf("------JUEGO INICIADO------\n");
 printf("JUGADOR B CONECTADO\n");
 strcpy(enviar,"JUGADOR A CONECTADO...");
 send(fd2, enviar, 1024,0);

//Ciclo para enviar y recibir mensajes
while(1){
recv(fd2,buf,1024,0);
 if(strcmp(&buf,"salir")==0){
 break;
 }else if (strcmp(&buf,"¡Ganaste!")==0){
     printf("%s\n",buf);
     break;
 }


 printf("La coordenada recibida del jugadorB es: (%s)\n",buf);
     if(A[buf[0]-'0'][buf[2]-'0'] == 0){ //si en la coordenada hay un 0 significa que no hay barco, así que continua el juego
        printf("Aqui no hay barco, ¡el jugadorB falló!\n");
    }else{ //si hay un uno se cambia el valor a cero ya que se ha hundido un barco y se recorre el arreglo para comprobar que aun haya más barcos.
        A[buf[0]-'0'][buf[2]-'0'] = 0;
        printf("Te han hundido un barco, ");
        int salir = 1;
        int aux=0;
        for(int i=0; i < 3; i++){
            for(int j=0; j<3; j++){
                if(A[i][j]==1){ //Si se encuentra un uno, el juego continúa
                    printf("pero sigue el juego.\n");
                    salir = 0;
                    aux++;
                    break;

                }

            }
            if(salir == 0){
                break;
            }
        }
        if(aux==0){
            printf("y ¡Has perdido el juego!\n");
            send(fd2,"¡Ganaste!",1024,0);
            send(fd2,"salir",1024,0);
            break;
        }
    }


 //El cliente recibe el mensaje del servidor
 printf("Escribir coordenada en formato X,Y: ");
 scanf("%*c%[^\n]",enviar2);
 send(fd2,enviar2,1024,0);
 if(strcmp(enviar2,"salir")==0){
 break;
  }
}
 close(fd2);
 close(fd);
return 0;
}
