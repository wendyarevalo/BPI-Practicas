#include <stdio.h>

int a;
int b;
int *ptrA;
int *ptrB;


int main(){
    a = 8;
    b = 6;

    printf("El entero A es %d\nEl entero B es %d\n",a,b);
    ptrA = &a;
    ptrB = &b;

    //XOR X^Y
    //0 0 -> 0
    //0 1 -> 1
    //1 0 -> 1
    //1 1 -> 0

    *ptrA = *ptrA^*ptrB;
    *ptrB = *ptrA^*ptrB;
    *ptrA = *ptrA^*ptrB;

    printf("\nCambiando...\nEl entero A ahora es %d\nEl entero B ahora es %d",a,b);

    return 0;
}
