#include <stdio.h>

int pot_rec(int,int);
int simula(int,int);

int main(){
    int a = 2;
    int b = 4;
    printf("El resultado de la potencia de %d a la %d, es: %d",a,b,pot_rec(a,b));
}

int pot_rec(int num, int pot){
    if(num == 0){
        return 0;
    }else if (pot == 0){
        return 1;
    }else if (pot == 1){
        return num;
    }else{
        return simula(num, pot_rec(num,pot-1));
    }
}

int simula(int num, int pot){
    if(pot != 0){
        return (num+simula(num,pot-1));
    }else{
        return 0;
    }
}
