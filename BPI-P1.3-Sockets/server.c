
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
//librerias

FILE *archivo; //se necesita para leer el archivo donde está el numero

void error(char *msg)
{
    perror(msg);
    exit(1);
}//esta funcion se llamara cuando haya errores

int main(int argc, char *argv[])
{

    int nums = 0; //variable para almacenar el numero a comparar
     int sockfd, newsockfd, portno, n, clilen; //sockfd y newsockdf descripción del socket, portno puerto que acepta conexiones, n valor de retorno de la lectura y escritura y clilen almacena el tamaño de la dirección del cliente
     char buffer[256];//Para leer los caracteres del mensaje
     struct sockaddr_in serv_addr, cli_addr;//sockaddr contiene la direccion a internet, se define en la libreria netinet
    //serv_addr es la direccion del servidor
    //cli_add es la direccion del cliente que conecta al servidor
     if (argc < 2) {
         fprintf(stderr,"ERROR, no port provided\n");
         exit(1);
     }
    //lectura del puerto en el que el socket servidor correra

     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0)
        error("ERROR opening socket");
    //Aqui es donde se crea el socket, con 3 parametros:
    //AF_INET Direccion de dominio del socket (direccion de internet, si fuera unix se usa AF_UNIX)
    //SOCK_STREAM tipo de socket, en este caso es de tipo stream(TCP), los caracteres se leen de forma continua. SOCK_DGRAM El otro tipo es de datagram(UDP) donde los mensajes se leen por pedacitos
    //Valor de retorno del socket, en caso de fallar en lugar de 0 se regresará -1

     bzero((char *) &serv_addr, sizeof(serv_addr));
    //La funcion bzero inicializa el buffer en 0, toma dos parametros:
    //un puntero al buffer
    //tamaño del buffer
     portno = atoi(argv[1]);
    //Puerto en que el que el socket escuchara convertido a entero
     serv_addr.sin_family = AF_INET;
    //estructura de tipo sockaddr_in que contiene la direccion de la familia
     serv_addr.sin_addr.s_addr = INADDR_ANY;
    //estructura de tipo sockaddr_in que contiene la direccion del host
     serv_addr.sin_port = htons(portno);
    //estructura de tipo sockaddr_in que contiene el numero de puerto. Se usa la funcion htons para convertirlo a orden de bytes de red

     if (bind(sockfd, (struct sockaddr *) &serv_addr,
              sizeof(serv_addr)) < 0)
              error("ERROR on binding");
    //la funcion bind liga a un socket con una direccion, en este caso del host y el puerto en el que el servdor correra.
    //Esta funcion tiene 3 parametros:
    //El descriptor del socket
    //Dirección a enlazar
    //El tamaño de la direccion que se enlazará

     listen(sockfd,5);
    //la funcion listen permite al proceso escuchar para recibir conexiones de sockets, para esto se recibe la descripción del socket y el numero máximo de sockets que se pueden conectar
     clilen = sizeof(cli_addr);
     newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
     if (newsockfd < 0)
          error("ERROR on accept");
    //La funcion accept bloquea el proceso hasta que un cliente se conecta, retorna un nuevo descriptor de socket, un puntero a la direccion del cliente al final de la coneccion y al final el tamaño de esta estructura


    archivo =fopen("num_servidor.txt","r");
    if(archivo == NULL){
        printf("Error al abrir el archivo");
        return 0;
    }

    while ( ! feof(archivo)){
        fscanf(archivo, "%i", &nums);
    }
    printf("El numero servidor es: %d",nums);
    //abriremos el archivo que contiene el numero para leerlo, si no se encuentra el archivo mandamos un error, si el archivo existe lo leemos y almacenamos el valor en la variable nums, luego lo mostramos.

    bzero(buffer,256);
    int valor;
     n = read(newsockfd,&valor,sizeof(valor));
     if (n < 0) error("ERROR reading from socket");
     printf("El numero cliente es: %d\n",valor);
    //lee la respuesta del socket, si el tamaño del mensaje es menor a cero entonces hay error, de lo contrario
    //Imprimirá el mensaje recibido por el cliente y hará la siguiente comparación para revisar con el numero que encontro en su archivo y descubrir el mayor de los dos

    if (valor>nums){
        printf("    El numero cliente es mayor %d, %d",valor,nums);
        n = write(newsockfd,"Tu numero es mayor",18);
    }else{
        printf("    El numero servidor es mayor %d, %d",valor,nums);
        n = write(newsockfd,"Mi numero es mayor",18);
    }


     if (n < 0) error("ERROR writing to socket");
     return 0;

    //dependiendo de la situación se escribe en el socket utilizando el nuevo descriptor de socket, enviando el buffer y por ultimo el tamaño del mensaje, en este caso 18 caracteres
}
