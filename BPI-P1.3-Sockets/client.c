#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
//librerias

FILE *archivo; //se necesita para leer el archivo donde está el numero

void error(char *msg)
{
    perror(msg);
    exit(0);
} //esta funcion se llamara cuando haya errores

int main(int argc, char *argv[])
{
    int sockfd, portno, n; //sockfd descripción del socket, portno puerto que acepta conexiones, n valor de retorno de la lectura y escritura
    int nums = 0; //variable para almacenar el numero a comparar

    struct sockaddr_in serv_addr; //sockaddr contiene la direccion a internet, se define en la libreria netinet
    //serv_addr es la direccion del servidor
    struct hostent *server; //puntero a la estructura de tipo hostent, que define una computadora en el internet, la definicion esta en la libreria netdb.h

    char buffer[256]; //Para leer los caracteres del mensaje
    if (argc < 3) {
       fprintf(stderr,"usage %s hostname port\n", argv[0]);
       exit(0);
    }
    //lectura del puerto en el que el socket cliente correra

    portno = atoi(argv[2]); //Puerto en que el que el socket escuchara convertido a entero
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        error("ERROR opening socket");
    //Aqui es donde se crea el socket, con 3 parametros:
    //AF_INET Direccion de dominio del socket (direccion de internet, si fuera unix se usa AF_UNIX)
    //SOCK_STREAM tipo de socket, en este caso es de tipo stream(TCP), los caracteres se leen de forma continua. SOCK_DGRAM El otro tipo es de datagram(UDP) donde los mensajes se leen por pedacitos
    //Valor de retorno del socket, en caso de fallar en lugar de 0 se regresará -1

    server = gethostbyname(argv[1]); //es una estructura que contiene el nombre del host en internet mediante la siguiente funcion de struct:
    //struct hostnet *gethostbyname(char *name)
    //Obtiene el nombre como un argumento y regresa un puntero a hostent que contiene la información del host
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }//si la estructura es nula no se puede localizar el host

    bzero((char *) &serv_addr,sizeof(serv_addr));
    //La funcion bzero inicializa el buffer en 0, toma dos parametros:
    //un puntero al buffer
    //tamaño del buffer
    serv_addr.sin_family = AF_INET;
    //estructura de tipo sockaddr_in que contiene la direccion de la familia
    bcopy((char *)server->h_addr,
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    //Copia la longitud de bytes del servidor
    serv_addr.sin_port = htons(portno);
    //estructura de tipo sockaddr_in que contiene el numero de puerto. Se usa la funcion htons para convertirlo a orden de bytes de red



    if (connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0)
        error("ERROR connecting");
    //la funcion connect es llamada por el cliente para establecer la conexion, para esto se necesitan los argumentos:
    //descripcion del socket
    //dirección del host al que se quiere conectar, incluyendo el puerto
    //y el tamaño de la dirección

    archivo =fopen("num_cliente.txt","r");
    if(archivo == NULL){
        printf("Error al abrir el archivo");
        return 0;
    }
    while ( ! feof(archivo)){
        fscanf(archivo, "%i", &nums);
    }
    printf("El numero cliente es: %d",nums);
    //abriremos el archivo que contiene el numero para leerlo, si no se encuentra el archivo mandamos un error, si el archivo existe lo leemos y almacenamos el valor en la variable nums, luego lo mostramos.


    n = write(sockfd,&nums,sizeof(nums));
    if (n < 0)
         error("ERROR writing to socket");
    //se escribe el mensaje en el socket, si el tamaño del mensaje es menor a 0 entonces no hay nada que escribir y tendremos un error

    bzero(buffer,256);
    n = read(sockfd,buffer,255);
    if (n < 0)
         error("ERROR reading from socket");
    //lee la respuesta del socket, si el tamaño del mensaje es menor a cero entonces hay error
    printf("%s\n",buffer);
    //imprimimos el mensaje
    return 0;
}
