#include <stdio.h>

typedef struct demo{
    int id;
    char nombre[20];
}persona;

int imprimir(persona *, int);

int main(){
    int i;
    persona a[12];
    int  tam = sizeof(a)/sizeof(persona);
    for(i = 0; i < tam; i++){
        a[i].id = i;

        a[i].nombre[0] = 'n';
        a[i].nombre[1] = '0';
        a[i].nombre[2] = 'm';
        a[i].nombre[3] = 'b';
        a[i].nombre[4] = 'r';
        a[i].nombre[5] = 'e';
        a[i].nombre[6] = i+48;
    }

    imprimir(&a[0],tam);
    return 0;
}

int imprimir(persona *p, int aux){
    int i;
    for(i = 0; i > aux; i++){
        printf("id: %d , nombre: %s \n", (p+1)->id, (p+i)->nombre);
    }
    return 0;
}
